# Docker image to build gitea

## Installation

```sh
docker pull gitea/build
```

## Usage

## Contributing

Fork -> Patch -> Push -> Pull Request

- `make test` run testsuite
- `make vendor` when adding new dependencies
- ... (for other development tasks, check the `Makefile`)

## Authors

* [Maintainers](https://github.com/orgs/go-gitea/people)
* [Contributors](https://github.com/go-gitea/tea/graphs/contributors)

## License

This project is under the MIT License. See the [LICENSE](LICENSE) file for the
full license text.
